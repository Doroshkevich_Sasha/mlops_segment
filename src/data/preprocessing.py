import numpy as np
import os
from skimage.io import imread
from skimage.transform import resize
from tqdm import tqdm

# Установка seed для numpy
seed = 42
np.random.seed = seed

# Пути к изображениям и маскам
TRAIN_IMAGE_PATH = 'C:/Users/user/repo_mlops/data/processed/IMG'
TRAIN_IMAGE_MASK = 'C:/Users/user/repo_mlops/data/processed/MASK'

# Получение списка файлов в директории
train_ids = next(os.walk(TRAIN_IMAGE_PATH))[2]

# Определение размеров изображений и количества каналов
IMG_WIDTH = 256
IMG_HEIGHT = 256
IMG_CHANNELS = 3
IMG_CHANNELS2 = 1

# Создание пустых массивов numpy для изображений и масок
X_train = np.zeros(
    (len(train_ids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS2), dtype=np.uint8
)
Y_train = np.zeros(
    (len(train_ids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS2), dtype=np.bool_
)

# Загрузка и обработка изображений и масок
for n, id_ in tqdm(enumerate(train_ids), total=len(train_ids)):
    img = imread(os.path.join(TRAIN_IMAGE_PATH, id_))[:, :, :IMG_CHANNELS]
    img = resize(
        img, (IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS2),
        mode='constant', preserve_range=True
    )
    X_train[n] = img  # Заполнение пустого массива X_train значениями img

    mask = imread(os.path.join(TRAIN_IMAGE_MASK, id_))[:, :, :IMG_CHANNELS2]
    mask = resize(
        mask, (IMG_HEIGHT, IMG_WIDTH), mode='constant',
        preserve_range=True
    )
    Y_train[n] = mask
