import random
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageEnhance
from sklearn.metrics import classification_report
from skimage.io import imread, imshow

preds_val = model.predict(X_train[int(X_train.shape[0]*0.9):], verbose=1)
preds_val_t = (preds_val > 0.5).astype(np.bool_)
preds_val_S = model.predict(X_train).flatten()
Y_train_f = Y_train.flatten()
preds_val_S_t = (preds_val_S > 0.5).astype(np.bool_)

report = classification_report(Y_train_f, preds_val_S_t)
print(report, '\n')

jac_index = 0
for i in range(len(preds_val_t)):
    intersect = 0
    union = 0
    x = preds_val_t[i].flatten()
    y = Y_train[int(X_train.shape[0]*0.9) + i].flatten()
    for j in range(len(x)):
        if x[j] == 1 and y[j] == 1 :
            intersect += 1
            union += 1
        elif (x[j] == 1 and y[j] == 0) or (x[j] == 0 and y[j] == 1):
            union += 1
    jac_index += intersect*100.0/union

print("Jaccard index:", jac_index / len(preds_val_t), " %")
#print('\n', "DICE score"dice_bce_loss(Y_train2, preds_val_t))

ix = random.randint(0, len(preds_val_t))
imshow(X_train[int(X_train.shape[0]*0.9) + ix])
plt.show()
imshow(np.squeeze(Y_train[int(X_train.shape[0]*0.9) + ix]))
plt.show()
imshow(np.squeeze(preds_val_t[ix]))
plt.show()