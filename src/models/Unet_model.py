import tensorflow as tf
import os
import numpy as np
from tqdm import tqdm
from skimage.io import imread, imshow
from skimage.transform import resize
import matplotlib.pyplot as plt
from tensorflow import keras
from keras.optimizers import Adam
from sklearn.metrics import classification_report
import click


@click.command()
@click.argument("input_paths", type=click.Path())
@click.argument("output_path", type=click.Path())
def train_model(input_paths, output_path):
 # Установка seed для numpy
 seed = 42
 np.random.seed = seed

 # Пути к изображениям и маскам
 TRAIN_IMAGE_PATH = 'input_paths'
 TRAIN_IMAGE_MASK = 'output_path'

 # Получение списка файлов в директории
 train_ids = next(os.walk(TRAIN_IMAGE_PATH))[2]

 # Определение размеров изображений и количества каналов
 IMG_WIDTH = 256
 IMG_HEIGHT = 256
 IMG_CHANNELS = 3
 IMG_CHANNELS2 = 1

 # Создание пустых массивов numpy для изображений и масок
 X_train = np.zeros(
     (len(train_ids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS2), dtype=np.uint8
 )
 Y_train = np.zeros(
     (len(train_ids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS2), dtype=np.bool_
 )

 # Загрузка и обработка изображений и масок
 for n, id_ in tqdm(enumerate(train_ids), total=len(train_ids)):
     img = imread(os.path.join(TRAIN_IMAGE_PATH, id_))[:, :, :IMG_CHANNELS]
     img = resize(
         img, (IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS2),
         mode='constant', preserve_range=True
     )
     X_train[n] = img  # Заполнение пустого массива X_train значениями img

     mask = imread(os.path.join(TRAIN_IMAGE_MASK, id_))[:, :, :IMG_CHANNELS2]
     mask = resize(
         mask, (IMG_HEIGHT, IMG_WIDTH), mode='constant',
         preserve_range=True
     )
     Y_train[n] = mask

 inputs = tf.keras.layers.Input((IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS2))
 s = tf.keras.layers.Lambda(lambda x: x / 255)(inputs)

 # Contraction path
 c1 = tf.keras.layers.Conv2D(
     32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same'
 )(s)
 c1 = tf.keras.layers.Dropout(0.1)(c1)
 c1 = tf.keras.layers.Conv2D(
     32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same'
 )(c1)
 p1 = tf.keras.layers.MaxPooling2D((2, 2))(c1)

 c2 = tf.keras.layers.Conv2D(
     64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same'
 )(p1)
 c2 = tf.keras.layers.Dropout(0.1)(c2)
 c2 = tf.keras.layers.Conv2D(
     64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same'
 )(c2)
 p2 = tf.keras.layers.MaxPooling2D((2, 2))(c2)

 c3 = tf.keras.layers.Conv2D(
     128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same'
 )(p2)
 c3 = tf.keras.layers.Dropout(0.2)(c3)
 c3 = tf.keras.layers.Conv2D(
     128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same'
 )(c3)
 p3 = tf.keras.layers.MaxPooling2D((2, 2))(c3)

 c4 = tf.keras.layers.Conv2D(
     256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same'
 )(p3)
 c4 = tf.keras.layers.Dropout(0.2)(c4)
 c4 = tf.keras.layers.Conv2D(
     256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same'
 )(c4)
 p4 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(c4)

 c5 = tf.keras.layers.Conv2D(
     512, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same'
 )(p4)
 c5 = tf.keras.layers.Dropout(0.3)(c5)
 c5 = tf.keras.layers.Conv2D(
     512, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same'
 )(c5)

 # Expansive path
 u6 = tf.keras.layers.Conv2DTranspose(
     256, (2, 2), strides=(2, 2), padding='same'
 )(c5)
 u6 = tf.keras.layers.concatenate([u6, c4])
 c6 = tf.keras.layers.Conv2D(
     256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same'
 )(u6)
 c6 = tf.keras.layers.Dropout(0.2)(c6)
 c6 = tf.keras.layers.Conv2D(
     256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same'
 )(c6)

 u7 = tf.keras.layers.Conv2DTranspose(
     128, (2, 2), strides=(2, 2), padding='same'
 )(c6)
 u7 = tf.keras.layers.concatenate([u7, c3])
 c7 = tf.keras.layers.Conv2D(
     128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same'
 )(u7)
 c7 = tf.keras.layers.Dropout(0.2)(c7)
 c7 = tf.keras.layers.Conv2D(
     128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same'
 )(c7)

 u8 = tf.keras.layers.Conv2DTranspose(
     64, (2, 2), strides=(2, 2), padding='same'
 )(c7)
 u8 = tf.keras.layers.concatenate([u8, c2])
 c8 = tf.keras.layers.Conv2D(
     64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same'
 )(u8)
 c8 = tf.keras.layers.Dropout(0.1)(c8)
 c8 = tf.keras.layers.Conv2D(
     64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same'
 )(c8)

 u9 = tf.keras.layers.Conv2DTranspose(
     32, (2, 2), strides=(2, 2), padding='same'
 )(c8)
 u9 = tf.keras.layers.concatenate([u9, c1], axis=3)
 c9 = tf.keras.layers.Conv2D(
     32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same'
 )(u9)
 c9 = tf.keras.layers.Dropout(0.1)(c9)
 c9 = tf.keras.layers.Conv2D(
     32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same'
 )(c9)

 outputs = tf.keras.layers.Conv2D(1, (1, 1), activation='sigmoid')(c9)

 model = tf.keras.Model(inputs=[inputs], outputs=[outputs])
 model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
 model.summary()

 callbacks = [
    # tf.keras.callbacks.EarlyStopping(patience=30, monitor='val_loss'),
    # tf.keras.callbacks.TensorBoard(log_dir='drive/MyDrive/logs_tb4',
    #                               histogram_freq=1, write_images=True)
 ]
 model.fit(
     X_train,
     Y_train,
     validation_split=0.1,
     batch_size=16,
     epochs=10,
     callbacks=callbacks,
 )

 preds_val = model.predict(X_train[int(X_train.shape[0] * 0.9):], verbose=1)
 preds_val_t = (preds_val > 0.5).astype(np.bool_)
 preds_val_S = model.predict(X_train).flatten()
 Y_train_f = Y_train.flatten()
 preds_val_S_t = (preds_val_S > 0.5).astype(np.bool_)

 report = classification_report(Y_train_f, preds_val_S_t)
 print(report, '\n')

 jac_index = 0
 for i in range(len(preds_val_t)):
     intersect = 0
     union = 0
     x = preds_val_t[i].flatten()
     y = Y_train[int(X_train.shape[0] * 0.9) + i].flatten()
     for j in range(len(x)):
         if x[j] == 1 and y[j] == 1:
             intersect += 1
             union += 1
         elif (x[j] == 1 and y[j] == 0) or (x[j] == 0 and y[j] == 1):
             union += 1
     jac_index += intersect * 100.0 / union

 jac = jac_index / len(preds_val_t)
 print("Jaccard index:", jac, " %")
 return ()


if __name__ == "__main__":
    train_model()
